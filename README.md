# README #

Cambios a librería VisaNet Android



### Versión 2.0.33 ###
- Soporte para espacios en alias
- Optimización de conexión

### Versión 2.0.20 ###

- Soporte para tokenización de tarjetas

### Versión 1.0.11 ###

- Cambio en logo Visa
- Cambio en aplicación de ejemplo

### Versión 1.0.10 ###

- Se agregó validación sobre los campos MerchantId y TransactionId (Numérico 9 posiciones).


### Versión 1.0.9 ###

- Fix para conectar a CyberSource Producción correctamente.

### Versión 1.0.8 ###

- Fix bug donde no se regresaba el ID de la transacción correctamente en el objeto PaymentInfo.