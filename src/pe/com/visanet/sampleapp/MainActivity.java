package pe.com.visanet.sampleapp;

import java.net.URI;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import pe.com.visanet.lib.VisaNetConfigurationContext;
import pe.com.visanet.lib.VisaNetPaymentActivity;
import pe.com.visanet.lib.VisaNetConfigurationContext.VisaNetCurrency;
import pe.com.visanet.lib.VisaNetPaymentInfo;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final String TAG = "VISANET";
	private static int REQUEST_CODE_PAYMENT = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		if (android.os.Build.VERSION.SDK_INT > 9) {
		    StrictMode.ThreadPolicy policy = 
		        new StrictMode.ThreadPolicy.Builder().permitAll().build();
		    StrictMode.setThreadPolicy(policy);
		}		
		
		setTitle("VisaNet");
		setContentView(R.layout.activity_main);
		setTheme(android.R.style.Theme_Dialog);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		final Button btnPayNow = (Button) findViewById(R.id.btnPayNow);

		if (btnPayNow != null) {
			btnPayNow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					try {

						Log.i(TAG, "on click Pay Now");
						EditText edtAmount = (EditText) findViewById(R.id.editTextAmount);
						// EditText edtPurchaseOrder = (EditText)
						// findViewById(R.id.editTextPurchaseOrder);
						CheckBox chkIsTesting = (CheckBox) findViewById(R.id.checkboxTesting);
						CheckBox chkUseToken = (CheckBox) findViewById(R.id.checkboxUseToken);
						boolean isTesting = chkIsTesting.isChecked();
						boolean useToken = chkUseToken.isChecked();

						double amount = Double.parseDouble(edtAmount.getText()
								.toString());
						String externalTransactionId = "my.external.id";
						String merchantId = isTesting ? "521040502"
								: "341198210"; // edtMerchantId.getText().toString();
						String userTokenId = useToken ? (isTesting ? "269c8764-2f39-453c-b11f-bbad462ecd71"
								: "41f4712e-3821-4e70-914d-d92b61b4cba1")
								: null;
						String endpoint = "https://devapi.vnforapps.com/api.tokenization/api/v2";
						String accessKeyId = isTesting ? "AKIAI4VNT2CSVWSTJV6Q"
								: "AKIAI737YRU5WIQ5W6JQ";
						String secretAccess = isTesting ? "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"
								: "hssNV8/TJHGs2FQUYTrufGmu/nzudtXU9fPOj5CO";

						// https://devapi.vnforapps.com/api.tokenization/api/v2/merchant/132272410/nextCounter
						String nextCounterUri = isTesting ? String
								.format("https://devapi.vnforapps.com/api.tokenization/api/v2/merchant/%s/nextCounter",
										merchantId)
								: String.format(
										"https://devapi.vnforapps.com/api.tokenization/api/v2/merchant/%s/nextCounter",
										merchantId);
						Log.i(TAG, nextCounterUri);

						DefaultHttpClient client = new DefaultHttpClient();
						Log.i(TAG, "Http Client created");
						HttpGet nextCounterGet = new HttpGet();
						nextCounterGet.setURI(new URI(nextCounterUri));
						String authorization = String.format("%s:%s",
								accessKeyId, secretAccess);
						String usernamePasswordEncoded = Base64.encodeToString(
								authorization.getBytes(), Base64.NO_WRAP);
						nextCounterGet.addHeader("Authorization", String
								.format("Basic %s", usernamePasswordEncoded));
						nextCounterGet.addHeader("Content-Type",
								"application/json");

						Log.i(TAG, "Get created");
						// response
						HttpResponse httpResponse = client
								.execute(nextCounterGet);
						int statusCode = httpResponse.getStatusLine()
								.getStatusCode();

						HttpEntity entity = httpResponse.getEntity();
						String transactionId = EntityUtils.toString(entity,
								"UTF-8");
						Log.i(TAG, transactionId);
						// String transactionId =
						// edtPurchaseOrder.getText().toString();

						// inicializar objeto VisaNetConfigurationContext
						VisaNetConfigurationContext ctx = new VisaNetConfigurationContext();
						if (isTesting) {
							ctx.setTesting(true);
							ctx.setEndPointURL(endpoint);
						} else {
							ctx.setEndPointURL("https://api.vnforapps.com/api.tokenization/api/v2");
						}
						// inicializar data
						HashMap<String, String> data = new HashMap<String, String>();
						ctx.setData(data);
						ctx.setAccessKeyId(accessKeyId);
						ctx.setSecretAccess(secretAccess);
						ctx.setCustomerFirstName("VisaNet");
						ctx.setCustomerLastName("Pruebas");
						ctx.setCustomerEmail("vndp@gmail.com");
						// ctx.setLanguage(VisaNetLanguage.SPANISH);
						ctx.setMerchantId(merchantId);
						ctx.setCurrency(VisaNetCurrency.PEN);
						ctx.setAmount(amount);
						ctx.setTransactionId(transactionId);
						ctx.setExternalTransactionId(externalTransactionId);
						ctx.setUserTokenId(userTokenId);
						// ctx.setMaxCardsPerToken(4); // 3 is default

						ctx.getMerchantDefinedData().put("field3", "movil");

						// iniciar actividad VisaNet
						Intent intent = new Intent(MainActivity.this,
								VisaNetPaymentActivity.class);
						intent.putExtra(
								VisaNetConfigurationContext.VISANET_CONTEXT,
								ctx);
						startActivityForResult(intent, REQUEST_CODE_PAYMENT);

					} catch (Exception e) {
						e.printStackTrace();
						Log.e(TAG, e.getLocalizedMessage());
					}
				}
			});
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PAYMENT) {
			if (resultCode == Activity.RESULT_OK
					|| resultCode == Activity.RESULT_CANCELED) {

				EditText edtProduct = (EditText) findViewById(R.id.editTextProducto);
				String product = edtProduct.getText().toString();

				if (data != null) {
					VisaNetPaymentInfo info = (VisaNetPaymentInfo) data
							.getSerializableExtra(VisaNetConfigurationContext.VISANET_CONTEXT);
					Log.i(TAG, "TransactionId: " + info.getTransactionId());
					String email = info.getEmail();
					String firstName = info.getFirstName();
					String lastName = info.getLastName();

					String codAccion = info.getData().get("CODACCION");
					Log.i(TAG, "codAccion: " + codAccion);
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(
							this);
					alertDialog.setTitle("Resultado");
					String infoData = "";
					for (String item : info.getData().keySet()) {
						infoData += String.format("%s: %s\n", item, info
								.getData().get(item));
					}

					String message = String.format("Resultado: %s\n"
							+ "Mensaje: %s\n" + "Data: %s\n"
							+ "Cliente: %s %s\n" + "Email: %s",
							info.getPaymentStatus(),
							info.getPaymentDescription(), infoData, firstName,
							lastName, email);
					alertDialog.setMessage(message);
					alertDialog.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									return;
								}
							});
					alertDialog.show();

					Toast.makeText(
							getApplicationContext(),
							"Payment Information received from VisaNet"
									+ info.getTransactionId(),
							Toast.LENGTH_LONG).show();
				}
			} else if (resultCode == Activity.RESULT_CANCELED) {
				Log.i(TAG, "Processing Error!!!.");

			}
		}
	}
}
